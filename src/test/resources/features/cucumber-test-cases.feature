Feature: As part of integration testing i would like to test different resource of REST-API

  Background: This feature file have test scenarios for GET,PUT,POST
    Given Setting up the APIClient "https://internal.api.vu.edu.au/oimrestinterface-test/api/oimService/v1"
    And With authentication detail username "oim" and password "14oim" and default userSession

  Scenario Outline: Successful API GET Call
    When I make API GET Call to "<path>" with path variable
    Then Validating GET Call Response with Random Value "<validvaluetomatch>"

    Examples: 
      | path                  | validvaluetomatch |
      | roles/RoleAdmin/users | RoleAdmin         |

  Scenario Outline: Successful API POST Call
    When I make API POST Call to "<path>" with body Fragment values of userId "<userId>" and defaultRole "<defaultRole>"
    Then Validating POST Call Response with Random Value "<validvaluetomatch>"

    Examples: 
      | path      | userId   | defaultRole | validvaluetomatch |
      | offerings | e5023984 | LMSAdv      | "success": "true" |

  Scenario Outline: Successful API PUT Call
    Given Setting up the d2l-backend-dev APIClient "https://internal.api.vu.edu.au/d2l-backend-dev/api/v1/"
    When I make API PUT Call to "<path>" with body Fragment values of template version "<versionTemplate>"
    Then Validating PUT Call Response with Random Value "<validvaluetomatch>"

    Examples: 
      | path                    | versionTemplate | validvaluetomatch |
      | D2L/2.0/templates/62416 |               1 | "version": 0      |
