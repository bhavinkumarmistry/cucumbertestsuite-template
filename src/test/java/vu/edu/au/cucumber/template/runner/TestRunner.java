package vu.edu.au.cucumber.template.runner;

import cucumber.api.CucumberOptions;

@CucumberOptions(features = "src/test/resources/features", glue = { "stepdefs" }, plugin = { "pretty",
		"html:target/cucumber-reports/cucumber-pretty", "json:target/cucumber-reports/CucumberTestReport.json",
		"rerun:target/cucumber-reports/rerun.txt" })
public class TestRunner {
}
