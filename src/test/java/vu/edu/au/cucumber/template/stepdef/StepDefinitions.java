package vu.edu.au.cucumber.template.stepdef;

import javax.ws.rs.core.Response;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import vu.edu.au.cucumber.http.client.APIClient;
import vu.edu.au.cucumber.http.client.Headers;
import vu.edu.au.cucumber.http.client.QueryParameters;
import vu.edu.au.cucumber.template.utility.Constant;
import vu.edu.au.cucumber.template.utility.ResourceSampleToString;

public class StepDefinitions {

	private APIClient apiClient = null;
	private Object response;

	String defaultUserSession;

	@Given("Setting up the APIClient \"([^\"]*)\"")
	public void initializeApiClient(String bashUrl) {
		apiClient = new APIClient(bashUrl);
	}

	@And("With authentication detail username \"([^\"]*)\" and password \"([^\"]*)\"")
	public void addAuthenticationDetail(String userName, String password) {
		if (apiClient == null)
			throw new RuntimeException(
					"APIClient is Null, Before setting authentication you must have API Client to intialized !!!");

		apiClient.setBasicAuthentication(userName, password);

		defaultUserSession = ResourceSampleToString.getResourceAsString("/samples/userSession.json");
	}

	@When("I make API GET Call to \"([^\"]*)\" with path variable")
	public void makingGetcall(String path) {

		QueryParameters queryParams = new QueryParameters();
		queryParams.addQueryParameters("userSession", defaultUserSession);

		Headers headers = new Headers();
		headers.addHeaders(Constant.ACCEPT_HEADER_KEY, Constant.ACCEPT_HEADER_VALUE);
		headers.addHeaders(Constant.CONTENT_TYPE_HEADER_KEY, Constant.CONTENT_TYPE_HEADER_VALUE);

		Response apiClientResponse = apiClient.get(path, queryParams, headers);

		response = apiClientResponse.readEntity(String.class);

		System.out.println(apiClientResponse.getStatusInfo() + " Response Status : " + response.toString());
	}

	@Then("Validating GET Call Response with Random Value \"([^\"]*)\"")
	public void validateGetCall(String responseToValidate) {
		Assert.assertNotNull("Get Call Response is Not Null", response);
		Assert.assertTrue(" Response Validation", response.toString().contains(responseToValidate));
	}

	@When("I make API POST Call to \"([^\"]*)\" with body Fragment values of userId \"([^\"]*)\" and defaultRole \"([^\"]*)\"")
	public void makingPostCall(String path, String userId, String defaultRole) {

		QueryParameters queryParams = new QueryParameters();
		queryParams.addQueryParameters("userSession", defaultUserSession);

		Headers headers = new Headers();
		headers.addHeaders(Constant.ACCEPT_HEADER_KEY, Constant.ACCEPT_HEADER_VALUE);
		headers.addHeaders(Constant.CONTENT_TYPE_HEADER_KEY, Constant.CONTENT_TYPE_HEADER_VALUE);

		String postBody = ResourceSampleToString.getResourceAsString("/samples/postRequestBody.json");
		postBody = postBody.replace("userIdTemplate", userId);
		postBody = postBody.replace("defaultRoleTemplate", defaultRole);

		System.out.println("UserSession : "+ defaultUserSession + " , Request Body : "+ postBody);
		
		Response apiClientResponse = apiClient.post(path, postBody, queryParams, headers);
		
		response = apiClientResponse.readEntity(String.class);
		
		System.out.println(apiClientResponse.getStatusInfo() + " Response Status : " + response.toString());
	}
	
	@Then("Validating POST Call Response with Random Value \"([^\"]*)\"")
	public void validatePostCall(String responseToValidate) {
		Assert.assertNotNull("POST Call Response is Not Null", response);
		Assert.assertTrue(" Response Validation", response.toString().contains(responseToValidate));
	}
	
	@Given("Setting up the d2l-backend-dev APIClient \"([^\"]*)\"")
	public void createSecondApiClient(String bashUrl){
		apiClient = new APIClient(bashUrl);
	}
	
	@When("I make API PUT Call to \"([^\"]*)\" with body Fragment values of template version \"([^\"]*)\"")
	public void makingPutCall(String path, String templateValue) {
		QueryParameters queryParams = new QueryParameters();
		queryParams.addQueryParameters("userSession", defaultUserSession);

		Headers headers = new Headers();
		headers.addHeaders(Constant.ACCEPT_HEADER_KEY, Constant.ACCEPT_HEADER_VALUE);
		headers.addHeaders(Constant.CONTENT_TYPE_HEADER_KEY, Constant.CONTENT_TYPE_HEADER_VALUE);

		String putBody = ResourceSampleToString.getResourceAsString("/samples/putRequestBody.json");
		putBody = putBody.replace("versionTemplateValue", templateValue);

		System.out.println("UserSession : " + defaultUserSession + " , Request Body : " + putBody);

		Response apiClientResponse = apiClient.post(path, putBody, queryParams, headers);

		response = apiClientResponse.readEntity(String.class);

		System.out.println(apiClientResponse.getStatusInfo() + " Response Status : " + response.toString());
	}

	@Then("Validating PUT Call Response with Random Value \"([^\"]*)\"")
	public void validatePutCall(String responseToValidate) {
		Assert.assertNotNull("POST Call Response is Not Null", response);
		Assert.assertTrue(" Response Validation", response.toString().contains(responseToValidate));
	}
}
