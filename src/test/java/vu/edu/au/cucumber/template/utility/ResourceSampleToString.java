package vu.edu.au.cucumber.template.utility;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class ResourceSampleToString {

	public static String getResourceAsString(String resouece) {

		return new ResourceSampleToString().readFile(resouece);
	}

	public String readFile(String path) {
		InputStream resourceAsStream = this.getClass().getResourceAsStream(path);
		String result;
		try {
			result = IOUtils.toString(resourceAsStream);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		return result;
	}

	public static void main(String[] args) {

		String userSessionFromFile = ResourceSampleToString.getResourceAsString("/samples/userSession.json");
		String requestBodyFromFile = ResourceSampleToString.getResourceAsString("/samples/postRequestBody.json");
		System.out.println("UserSession : " + userSessionFromFile + " , Request Body : " + requestBodyFromFile);
	}
}
