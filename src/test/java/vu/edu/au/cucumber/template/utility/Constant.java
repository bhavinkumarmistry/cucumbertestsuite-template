package vu.edu.au.cucumber.template.utility;

public class Constant {

	public static final String ACCEPT_HEADER_KEY = "Accept";
	public static final String ACCEPT_HEADER_VALUE = "application/json";

	public static final String CONTENT_TYPE_HEADER_KEY = "Content-Type";
	public static final String CONTENT_TYPE_HEADER_VALUE = "application/json";
}
